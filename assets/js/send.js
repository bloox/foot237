$(function(){
    $('#send_mail').on('submit', function(e){
    e.preventDefault();
    $.post('https://hooks.slack.com/services/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', 
        JSON.stringify({"text" : $('#defaultForm-email').val()}), 
        function(data, status, xhr){
            if(status == "success"){
                $('#modalLoginForm').modal('toggle');
                $('#okEmail').show(0).delay(5000).hide(0);
            }
        }).fail(function (xhr, status, error) {
            $('#modalLoginForm').modal('toggle');
            $('#koEmail').show(0).delay(5000).hide(0);
        }); 
    });
});